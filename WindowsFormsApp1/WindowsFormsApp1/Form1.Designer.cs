﻿namespace WindowsFormsApp1 {
    partial class btn_add {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbl_firstnumber = new System.Windows.Forms.Label();
            this.lbl_secondnumber = new System.Windows.Forms.Label();
            this.txt_firstnumber = new System.Windows.Forms.TextBox();
            this.txt_secondnumber = new System.Windows.Forms.TextBox();
            this.add = new System.Windows.Forms.Button();
            this.btn_quit = new System.Windows.Forms.Button();
            this.btn_subtract = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_firstnumber
            // 
            this.lbl_firstnumber.AutoSize = true;
            this.lbl_firstnumber.Location = new System.Drawing.Point(121, 136);
            this.lbl_firstnumber.Name = "lbl_firstnumber";
            this.lbl_firstnumber.Size = new System.Drawing.Size(66, 13);
            this.lbl_firstnumber.TabIndex = 0;
            this.lbl_firstnumber.Text = "First Number";
            this.lbl_firstnumber.Click += new System.EventHandler(this.lbl_firstnumber_Click);
            // 
            // lbl_secondnumber
            // 
            this.lbl_secondnumber.AutoSize = true;
            this.lbl_secondnumber.Location = new System.Drawing.Point(103, 172);
            this.lbl_secondnumber.Name = "lbl_secondnumber";
            this.lbl_secondnumber.Size = new System.Drawing.Size(84, 13);
            this.lbl_secondnumber.TabIndex = 1;
            this.lbl_secondnumber.Text = "Second Number";
            this.lbl_secondnumber.Click += new System.EventHandler(this.lbl_secondnumber_Click);
            // 
            // txt_firstnumber
            // 
            this.txt_firstnumber.Location = new System.Drawing.Point(193, 133);
            this.txt_firstnumber.Name = "txt_firstnumber";
            this.txt_firstnumber.Size = new System.Drawing.Size(100, 20);
            this.txt_firstnumber.TabIndex = 2;
            // 
            // txt_secondnumber
            // 
            this.txt_secondnumber.Location = new System.Drawing.Point(193, 169);
            this.txt_secondnumber.Name = "txt_secondnumber";
            this.txt_secondnumber.Size = new System.Drawing.Size(100, 20);
            this.txt_secondnumber.TabIndex = 3;
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(78, 254);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(75, 23);
            this.add.TabIndex = 4;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_quit
            // 
            this.btn_quit.Location = new System.Drawing.Point(304, 253);
            this.btn_quit.Name = "btn_quit";
            this.btn_quit.Size = new System.Drawing.Size(75, 23);
            this.btn_quit.TabIndex = 5;
            this.btn_quit.Text = "Quit";
            this.btn_quit.UseVisualStyleBackColor = true;
            this.btn_quit.Click += new System.EventHandler(this.btn_quit_Click);
            // 
            // btn_subtract
            // 
            this.btn_subtract.Location = new System.Drawing.Point(193, 253);
            this.btn_subtract.Name = "btn_subtract";
            this.btn_subtract.Size = new System.Drawing.Size(75, 23);
            this.btn_subtract.TabIndex = 6;
            this.btn_subtract.Text = "Subtract";
            this.btn_subtract.UseVisualStyleBackColor = true;
            this.btn_subtract.Click += new System.EventHandler(this.btn_subtract_Click);
            // 
            // btn_add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_subtract);
            this.Controls.Add(this.btn_quit);
            this.Controls.Add(this.add);
            this.Controls.Add(this.txt_secondnumber);
            this.Controls.Add(this.txt_firstnumber);
            this.Controls.Add(this.lbl_secondnumber);
            this.Controls.Add(this.lbl_firstnumber);
            this.Name = "btn_add";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_firstnumber;
        private System.Windows.Forms.Label lbl_secondnumber;
        private System.Windows.Forms.TextBox txt_firstnumber;
        private System.Windows.Forms.TextBox txt_secondnumber;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button btn_quit;
        private System.Windows.Forms.Button btn_subtract;
    }
}

