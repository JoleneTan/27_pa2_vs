﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1 
{
    public partial class btn_add : Form 
    {
        int FirstNumber, SecondNumber, Sum, Subtract;

        public btn_add() 
        {
            InitializeComponent();
        }

        private void lbl_firstnumber_Click(object sender, EventArgs e) 
        {

        }

        private void lbl_secondnumber_Click(object sender, EventArgs e) 
        {

        }

        private void btn_subtract_Click(object sender, EventArgs e) 
        {
            FirstNumber = int.Parse(txt_firstnumber.Text);
            SecondNumber = int.Parse(txt_secondnumber.Text);
            Subtract = FirstNumber - SecondNumber;
            MessageBox.Show("The difference between the two numbers is " + Subtract.ToString());
        }

        private void button1_Click(object sender, EventArgs e) 
        {
            FirstNumber = int.Parse(txt_firstnumber.Text);
            SecondNumber = int.Parse(txt_secondnumber.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of two numbers is " + Sum.ToString());
        }

        private void btn_quit_Click(object sender, EventArgs e) 
        {
            Application.Exit();
        }
    }
}
